package com.example.repeat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRepeatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRepeatApplication.class, args);
    }

}
