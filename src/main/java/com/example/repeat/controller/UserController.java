package com.example.repeat.controller;

import com.example.repeat.annotation.RepeatSubmit;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @RepeatSubmit
    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }
}
